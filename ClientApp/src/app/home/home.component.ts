import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {  
  form: FormGroup;
  
  constructor(private formBuilder: FormBuilder) { }
  ngOnInit(): void {
    this.form = new FormGroup({
      jobSkillWeightageRequired: this.formBuilder.array([this.createSkillSetRequiredFormGroup()])
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
    // this.postService.create(this.form.value).subscribe(res => {
    //      console.log('Post created successfully!');
    //      this.router.navigateByUrl('post/index');
    // })
  }
  private createSkillSetRequiredFormGroup(): FormGroup {
    return new FormGroup({
      'operand': new FormControl(0, Validators.required)
    })
  }
  public addSkillSetRequiredFormGroup() {
    const jobSkillWeightageRequired = this.form.get('jobSkillWeightageRequired') as FormArray
    jobSkillWeightageRequired.push(this.createSkillSetRequiredFormGroup())
  }
  public removeOrClearSkillSetRequired(i: number) {
    const jobSkillWeightageRequired = this.form.get('jobSkillWeightageRequired') as FormArray
    if (jobSkillWeightageRequired.length > 1) {
      jobSkillWeightageRequired.removeAt(i)
    } else {
      jobSkillWeightageRequired.reset()
    }
  }
}
